//***** Modules goes here *****//
const express = require('express');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express.Router();
//***** ///// *****//

//***** Distributing requests *****//

//~~ Get All deals on user Screen ~~//
const getAllDealsModule = require('./allDeals');
app.use('/getAll', getAllDealsModule);

//~~ Get All deals on user Screen ~~//
const searchModule = require('./search');
app.use('/search', searchModule);

module.exports = app;