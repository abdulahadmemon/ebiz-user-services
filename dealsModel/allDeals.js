//***** Modules goes here *****//
const express = require('express');
const Joi = require('joi');
const DealsData = require('./schema');
const StoreData = require('../partners/schema');
const { UserData } = require('../userModel/schema');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express.Router();
//***** ///// *****//

//***** Post Request for Login *****//
app.post('/', (req, res)=> {
    const { error } = validateData(req.body);
    if(error) {
        var errors = {
            success:false,
            msg:error.name, 
            data:error.details[0].message
        };
        res.send(errors);
        return;
    }
    
    checkDeal(req.body).then((response)=> {
        // console.log(response);
        if(!response[0]) {
            var errors = {
                success:false,
                msg:'No Deals Found', 
                data:{}
            };
            res.send(errors);
        }
        else {
            var success = {
                success:true,
                msg:'Deals Found', 
                data:response
            };
            res.send(success);
        }
    });
});
//***** ///// *****//

//***** User login data validation function *****//
function validateData(id) {
    const schema = Joi.object().keys({
        userId: Joi.string().required()
    });
    return Joi.validate(id, schema);
}
//***** ///// *****//

async function checkDeal(body) {
    const userStores = await UserData.findById(body.userId);
    
    var deals = await DealsData
    .find()
    .and( [ {storeId:userStores.liked_stores} ] )
    .sort({createdDate: -1});

    var arrTmp = [];
    for(var i=0; i < deals.length; i++) {
        var storeName = await StoreData.findById(deals[i].storeId).select('companyName');
        // var tmp = Object.assign({}, deals[i], storeName);
        var tmp = {
            _id: deals[i]._id,
            name: deals[i].name,
            description: deals[i].description,
            startDate: deals[i].startDate,
            endDate: deals[i].endDate,
            storeId: deals[i].storeId,
            image: deals[i].image,
            createdDate: deals[i].createdDate,
            storeName: storeName.companyName
        };
        arrTmp.push(tmp);
        if((i+1) == deals.length) {
            console.log('complete');
            console.log(arrTmp[0]);
            return (arrTmp);
        }
    }
}

module.exports = app;