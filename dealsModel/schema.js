const mongoose = require('mongoose');

//***** User Document Schema *****//
const uploadSchemaObj = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    startDate: {
        type: String,
        required: true
    },
    endDate: {
        type: String,
        required: true
    },
    storeId: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    createdDate:{ type:Date, default:Date.now }
});

const Schema = mongoose.model('deals', uploadSchemaObj);
//***** ///// *****//

module.exports = Schema;