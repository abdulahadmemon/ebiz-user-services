//***** Modules goes here *****//
const express = require('express');
const Joi = require('joi');
const DealsData = require('./schema');
const {UserData} = require('../userModel/schema');
const auth = require('../middleware/auth');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express.Router();
//***** ///// *****//

//***** Post Request for Login *****//
app.post('/', auth, (req, res)=> {
    const { error } = validateData(req.body);
    if(error) {
        var errors = {
            success:false,
            msg:error.name, 
            data:error.details[0].message
        };
        res.send(errors);
        return;
    }
    
    checkDeal(req.body).then((response)=> {
        // console.log(response);
        if(!response[0]) {
            var errors = {
                success:false,
                msg:'No Deals Found', 
                data:[]
            };
            res.send(errors);
        }
        else {
            var success = {
                success:true,
                msg:'Deals Found', 
                data:response
            };
            res.send(success);
        }
    });
});
//***** ///// *****//

//***** User login data validation function *****//
function validateData(id) {
    const schema = Joi.object().keys({
        userId: Joi.string().required(),
        searchTerm: Joi.string().required()
    });
    return Joi.validate(id, schema);
}
//***** ///// *****//

async function checkDeal(body) {
    const userStores = await UserData.findById(body.userId);
    
    const deals = await DealsData
    .find()
    .or( [ { name: { '$regex' : body.searchTerm, '$options' : 'i' } }, 
            { description: { '$regex' : body.searchTerm, '$options' : 'i' } } 
         ] )
    .and( [ {storeId:userStores.liked_stores} ] )
    .sort({createdDate: -1});
    return (deals);
}

module.exports = app;