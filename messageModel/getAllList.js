//***** Modules goes here *****//
const express = require('express');
const Joi = require('joi');
const MessageData = require('./schema');
const DealData = require('../dealsModel/schema');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express();
//***** ///// *****//

//***** Post Request for Login *****//
app.post('/', (req, res)=> {
    const { error } = validateMessageData(req.body);
    if(error) {
        var errors = {
            success:false,
            msg:error.name,
            data:error.details[0].message
        };
        res.send(errors);
        return;
    }
    
    checkMessageList(req.body).then((response)=> {
        if(response.length == 0) {
            var empty = {
                empty:false,
                msg:'No chat(s) for this user.',
                data:response
            };
            res.send(empty);
        }
        else {
            var success = {
                success:true,
                msg:'Data found.',
                data:response
            };
            res.send(success);
        }
    });
});
//***** ///// *****//

//***** User login data validation function *****//
function validateMessageData(data) {
    const schema = Joi.object().keys({
        userId: Joi.string().required()
    });
    return Joi.validate(data, schema);
}
//***** ///// *****//

async function checkMessageList(body) {
    
    const messages = await MessageData
    .find()
    .and([{userId:body.userId}])
    .sort({lastupdate: -1});
    let tmp = [];
    
    if(messages.length == 0){
        return (tmp);
    }
    else {
        for(var i = 0; i < messages.length; i++) {
            var imagePath = await getImage(messages[i].dealId);
            if(imagePath != null){
                tmp.push(messages[i]);
            }
            if((i+1) == messages.length) {
                console.log((i+1) +'=='+ tmp.length, 'completed');
                console.log("~~tmp~~");
                console.log(tmp);
                return (tmp);
            }
        }
    }
    // return (imagePath.image);
    // return (messages);
}
async function getImage(dealId) {
    const imagePath = await DealData.findById(dealId);
    
    if(imagePath != null)
        return imagePath.image;
    else
        return imagePath;
}


module.exports = app;