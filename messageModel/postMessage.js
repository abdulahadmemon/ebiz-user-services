//***** Modules goes here *****//
const express = require('express');
const Joi = require('joi');
const MessageData = require('./schema');
const DealsData = require('../dealsModel/schema');
const PartnerData = require('../partners/schema');
const auth = require('../middleware/auth');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express();
//***** ///// *****//

//***** Import & Initialize Firebase-Admin  *****//
const admin = require("firebase-admin");
const serviceAccount = require("../config/smartlybizshop-firebase-adminsdk.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://smartlybizshop.firebaseio.com"
});
//***** ///// *****//

//***** Post Request for Login *****//
app.post('/', auth, (req, res)=> {
    req.body = JSON.parse(Object.keys(req.body)[0]);
    const { error } = validateMessageData(req.body);
    if(error) {
        var errors = {
            success:false,
            msg:error.name,
            data:error.details[0].message
        };
        res.send(errors);
        return;
    }
    
    checkMessageIfExist(req.body).then((response)=> {
        console.log('level 1');
        if(response.length != 0)
            postMessageIfExist(response[0]._id, req.body).then((result)=> {
                console.log(result);
                if(result == 2 || result == 3) {
                    var errors = {
                        success:false,
                        msg:'There was an error posting this message.', 
                        data:{}
                    };
                    res.send(errors);
                }
                else {
                    checkMessageIfExist(req.body).then((stores)=> {
                        console.log('level 2');
                        // console.log(stores);
                        var success = {
                            success:true,
                            msg:'Message posted successfully.', 
                            data:stores[0]
                        };
                        // console.log("ye raha",stores[0].userId, stores[0].dealId)
                        sendPushNoti(stores[0], req.body.message);
                        res.send(stores[0]);
                    });
                }
            });
        else
            postMessageIfNotExist(req.body).then((result)=> {
                if(result == 500) {
                    var errors = {
                        success:false,
                        msg:'There was an error posting this message.', 
                        data:{}
                    };
                    res.send(errors);
                }
                else {
                    var success = {
                        success:true,
                        msg:'Message posted successfully.', 
                        data:result
                    };
                    // console.log("ye raha",stores[0].userId, stores[0].dealId)
                    sendPushNoti(result,req.body.message);
                    res.send(success);
                }
            });
    });
});
//***** ///// *****//

//***** User login data validation function *****//
function validateMessageData(messageData) {
    const schema = Joi.object().keys({
        userId: Joi.string().required(),
        storeId: Joi.string().required(),
        dealId: Joi.string().required(),
        message:Joi.any(),
        // readstatus: Joi.number(),
        lastupdate:Joi.date()

    });
    return Joi.validate(messageData, schema);
}
//***** ///// *****//

async function checkMessageIfExist(body) {
    // return new Promise((res)=> {
        // console.log('checkMessageIfExist Body: ',body);
        // console.log(body);
        const messages = await MessageData
        .find()
        .and([{userId:body.userId, storeId: body.storeId, dealId: body.dealId}]);
        return messages;
    // });
}

async function postMessageIfExist(_id, body) {
    console.log('postMessageIfExist Body: ',body);
    body.message.status= 1;
    // console.log("ye raha body",body.message);
    const result = await MessageData.updateMany(
        { _id: _id },
        { $push: { message: body.message },$set :{lastupdate:body.message.time} }
        
    );
   console.log(_id, result); 
    if(result.ok == 1 && result.nModified == 1) 
        return 1; // 1 = store added
    else if(result.ok == 1 && result.nModified == 0)
        return 2; // 2 = store already subscibed
    else
        return 3;
}

function postMessageIfNotExist(body) {

    return new Promise((res)=> {
        body.lastupdate = body.message.time;
        body.message.status = 1;
        var data = body;
        // console.log("new message has been arrived",body);
        const newMessage = new MessageData(data);
        const save = newMessage.save(function(err) {
            if (err) {
                console.log(err);
                return res(500);}
            else {
                // console.log(newMessage)
                return res(newMessage);}
        });
    });
}

async function sendPushNoti(data, message) {
    // console.log('===sendPushNoti===',data.userId);
    // console.log(data);
    const store = await PartnerData
    .findById(data.storeId);
    const deal = await DealsData
    .findById(data.dealId);

    var registrationToken = [store.gcm_id];
    // console.log(registrationToken);
    var options = {
        priority: "high",
        timeToLive: 60 * 60,
        contentAvailable: true,

    };
    var payload = {
        notification: {
            title: 'You have a message for "'+deal.name+'"',
            body: message.text,
            sound:'default'
        },
        data: {
            coldstart: 'false',
            dismissed: 'false',
            foreground: 'false',
            type:'new message',
            storeId:data.storeId,
            dealId:data.dealId,
            userId:data.userId
        }
    };
    // console.log('noti: ', payload);
    admin.messaging().sendToDevice(registrationToken, payload, options)
    .then(
        (response)=> {
            // console.log("Successfully sent message:", response);
        }
    )
    .catch(
        (error)=> {
            console.log("Error sending message:", error);
        }
    );
}
module.exports = app;