//***** Modules goes here *****//
const express = require('express');
const Joi = require('joi');
const DealData = require('../dealsModel/schema');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express();
//***** ///// *****//

//***** Post Request for Login *****//
app.post('/', (req, res)=> {
    const { error } = validateMessageData(req.body);
    if(error) {
        var errors = {
            success:false,
            msg:error.name,
            data:error.details[0].message
        };
        res.send(errors);
        return;
    }
    
    checkImagePath(req.body).then((response)=> {
        var success = {
            success:true,
            msg:'Data found.',
            data:response
        };
        res.send(success);
    });
});
//***** ///// *****//

//***** User login data validation function *****//
function validateMessageData(data) {
    const schema = Joi.object().keys({
        dealId: Joi.string().required()
    });
    return Joi.validate(data, schema);
}
//***** ///// *****//

async function checkImagePath(body) {
    
    const imagePath = await DealData.findById(body.dealId);
    const tmp = {image: imagePath.image, dealName: imagePath.name};
    return (tmp);
}

module.exports = app;