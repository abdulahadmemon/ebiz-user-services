//***** Modules goes here *****//
const express = require('express');
const Joi = require('joi');
const MessageData = require('./schema');
const PartnerData = require('../partners/schema');
const DealData = require('../dealsModel/schema');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express();
//***** ///// *****//

//***** Post Request for Login *****//
app.post('/', (req, res) => {

    const { error } = validateMessageData(req.body);
    if (error) {
        var errors = {
            success: false,
            msg: error.name,
            data: error.details[0].message
        };
        res.send(errors);
        return;
    }
    // console.log("hello",req.body)
    checkMessageIfExist(req.body).then((response) => {
        var success = {
            success: true,
            msg: '',
            data: response
        };
        res.send(success);
    });
});
//***** ///// *****//

//***** User login data validation function *****//
function validateMessageData(messageData) {
    const schema = Joi.object().keys({
        userId: Joi.string().required(),
        storeId: Joi.string().required(),
        dealId: Joi.string().required(),
        message: Joi.any()
    });
    return Joi.validate(messageData, schema);
}
//***** ///// *****//

async function checkMessageIfExist(body) {
    // return new Promise((res)=> {
    var messagedata = [];
    const messages = await MessageData
        .find()
        .and([{ userId: body.userId, storeId: body.storeId, dealId: body.dealId }]);
    // console.log("messa",messages[0]._id)
    if (messages.length > 0) {
        const store = await PartnerData.findById(messages[0].storeId);
        const deal = await DealData.findById(messages[0].dealId);
        // console.log("asasas",messages[0]);
        // console.log("store",body);
        // console.log(messages[0].message.length)
        for (var i = 0; i < messages[0].message.length; i++) {
            console.log(messages[0].message[i]);
            if (messages[0].message[i].type === 'shopkeeper') {
                if (messages[0].message[i].status === 1 || !messages[0].message[i].status) {
                    messages[0].message[i].status = 2;

                    // console.log(result);
                }
                else {
                    console.log("no unread");
                }
            }
            else {
                console.log("no unread");
            }
        }
        const result = await MessageData.updateMany(
            { _id: messages[0]._id },
            { $set: { message:messages[0].message }}

        );
        console.log(messages);
        // const result = await MessageData.updateOne(
        //     { _id: messages[0]._id },
        //     { $set: { message: "messages" } }

        // );
        // console.log(result);
        messages[0].storeName = store.companyName;
        messages[0].dealImage = deal.image || 'deals\Hp 1200 test-1549549326303.jpg';
        messages[0].dealName = deal.name;
        messages[0].dealDate = deal.createdDate;

        return (messages[0]);
    }
    else {
        // console.log('Not exist');
        const store = await PartnerData.findById(body.storeId);
        const deal = await DealData.findById(body.dealId);
        // console.log('store: ', store);
        const tmp = {
            storeName: store.companyName,
            dealImage: deal.image,
            dealName: deal.name,
            dealDate: deal.createdDate,
            message: []
        };

        return (tmp);
    }
    // setTimeout(()=> {
    // return messages;
    // },200);

    // });
}

module.exports = app;