const mongoose = require('mongoose');

//***** User Document Schema *****//
const messagesSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    storeId: {
        type: String,
        required: true
    },
    dealId: {
        type: String,
        required: true
    },
    dealName: {
        type: String,
    },
    storeName: {
        type: String,
    },
    lastupdate:{
        type:Date,
        required:true
    },
    // readstatus:{
    //     type:Number,
    //     required:true
    // },

    message:Array,
    createdDate:{ type:Date, default:Date.now }
});

const MessageData = mongoose.model('messages', messagesSchema);
//***** ///// *****//

module.exports = MessageData; 