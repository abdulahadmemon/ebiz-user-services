//***** Modules goes here *****//
const express = require('express');
const Joi = require('joi');
const {UserData} = require('./schema');
const PartnerData = require('../partners/schema');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express();
//***** ///// *****//

//***** Post Request for Login *****//
app.post('/', (req, res)=> {
    const { error } = validateUserData(req.body);
    if(error) {
        var errors = {
            success:false,
            msg:error.name,
            data:error.details[0].message
        };
        res.send(errors);
        return;
    }
    
    findStores(req.body).then((response)=> {
        if(response == 500) {
            var errors = {
                success:false,
                msg:'No User Found', 
                data:''
            };
            res.send(errors);
        }
        else {
            var success = {
                success:true,
                msg:'User Found', 
                data:response
            };
            res.send(success);
        }
    });
});
//***** ///// *****//

//***** User login data validation function *****//
function validateUserData(userData) {
    const schema = Joi.object().keys({
        userId: Joi.string()
    });
    return Joi.validate(userData, schema);
}
//***** ///// *****//

//***** Find User and return function *****//
async function findStores(body) {
    const user = await UserData.findById(body.userId);
    
    const partners = await PartnerData
    .find()
    .and( [ {_id:user.liked_stores} ] )
    .sort({createdDate: -1});
    return (partners);
}
//***** ///// *****//

module.exports = app;