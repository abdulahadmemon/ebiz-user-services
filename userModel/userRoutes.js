//***** Modules goes here *****//
const express = require('express');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express.Router();
//***** ///// *****//

//***** Distributing requests *****//

//~~ Signup ~~//
const signupModule = require('./signup');
app.use('/signup', signupModule);

//~~ Login ~~//
const loginModule = require('./login');
app.use('/login', loginModule);
//***** ///// *****//

//~~ Edit Profile ~~//
const editModule = require('./editProfile');
app.use('/edit', editModule);
//***** ///// *****//

//~~ Edit Profile ~~//
const subscribedModule = require('./subscribedStores');
app.use('/mySubscription', subscribedModule);
//***** ///// *****//

//~~ Update Gcm Profile ~~//
const updateGcmModule = require('./updateGcm');
app.use('/updategcm', updateGcmModule);
//***** ///// *****//

//~~ Get user Profile ~~//
const getUser = require('./getUser');
app.use('/getUser', getUser);
//***** ///// *****//
module.exports = app;