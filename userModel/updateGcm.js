//***** Modules goes here *****//
const express = require('express');
const Joi = require('joi');
const {UserData} = require('./schema');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express();
//***** ///// *****//

//***** Post Request for Signup *****//
app.post('/', (req, res)=> {
    const { error } = validateUserData(req.body);
    if(error) {
        var errors = {
            success:false,
            msg:error.name, 
            data:error.details[0].message
        };
        res.send(errors);
        return;
    }

    updateGCM(req.body).then (
        (response)=> {
            const requestData = {
                success: true,
                msg: 'GCM and Platform updated successfully.',
            };
            res.send(requestData);
        }
    );
});
//***** ///// *****//

//***** User signup data validation function *****//
function validateUserData(userData) {
    const schema = Joi.object().keys({
        userId: Joi.string(),
        gcm_id: Joi.string(),
        platform: Joi.string()
    });
    return Joi.validate(userData, schema);
}
//***** ///// *****//

//***** Initialing and saving data *****//
async function updateGCM(userData) {
    // const user = new UserData(userData);
    // const result = user.save(function(err) {
    //     console.log('function');
    //     if (err) {return res(500);}
    //     else {return res(200);}
    // });
    const user = await UserData.findByIdAndUpdate(userData.userId, {
        $set: {
            gcm_id: userData.gcm_id,
            platform: userData.platform 
        }
    });
}
//***** ///// *****//

module.exports = app;