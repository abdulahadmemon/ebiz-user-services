//***** Modules goes here *****//
const express = require('express');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express.Router();
//***** /////\\\\\ *****//

//***** Distributing requests *****//

//~~ Get All Partners ~~//
const PartnerModule = require('./getPartnerList');
app.use('/getAll', PartnerModule);
//***** /////\\\\\ *****//

const SubscribeModule = require('./subscribe');
app.use('/subscribe', SubscribeModule);

const getNameModule = require('./getNames');
app.use('/getStoreNames', getNameModule);


//~~ Update Gcm Profile ~~//
const unsubscribeModule = require('./unsubscribeStore');
app.use('/unsubscribeStore', unsubscribeModule);
//***** /////\\\\\ *****//

module.exports = app;