//***** Modules goes here *****//
const express = require('express');
const UserData = require('./schema');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express();
//***** ///// *****//

//***** Post Request for Login *****//
app.get('/', (req, res)=> {
    UserData.find({}, function(err, users){
        res.send(
            {
                success:true,
                msg:'All Dealers List',
                data: users
            }
        );
    });
    // const dealers = getPartners();
    
});
//***** ///// *****//

async function getPartners() {
    const data = await UserData.find({});
    console.log(data);
    return data;
}
module.exports = app;