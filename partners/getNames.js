//***** Modules goes here *****//
const express = require('express');
const Joi = require('joi');
const PartnersData = require('./schema');
const {UserData} = require('../userModel/schema');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express.Router();
//***** ///// *****//

//***** Post Request for Login *****//
app.post('/', (req, res)=> {
    
    const { error } = validateData(req.body);
    if(error) {
        var errors = {
            success:false,
            msg:error.name, 
            data:error.details[0].message
        };
        res.send(errors);
        return;
    }
    
    checkStore(req.body).then((response)=> {
        // console.log(response);
        if(!response[0]) {
            var errors = {
                success:false,
                msg:'No Stores Found', 
                data:[]
            };
            res.send(errors);
        }
        else {
            var success = {
                success:true,
                msg:'Store Found', 
                data:response
            };
            res.send(success);
        }
    });
});
//***** ///// *****//

//***** User login data validation function *****//
function validateData(data) {
    const schema = Joi.object().keys({
        userId: Joi.string().required()
    });
    return Joi.validate(data, schema);
}
//***** ///// *****//

async function checkStore(body) {
    const user = await UserData.findById(body.userId);

    const stores = await PartnersData
    .find()
    .and( [ {_id:user.liked_stores} ] )
    .select({companyName:1, _id:1})
    .sort({createdDate: -1});
    return (stores);
}

module.exports = app;