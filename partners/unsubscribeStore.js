//***** Modules goes here *****//
const express = require('express');
const Joi = require('joi');
const {UserData} = require('../userModel/schema');
const PartnerData = require('./schema');
const auth = require('../middleware/auth');
var _ = require('lodash');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express();
//***** ///// *****//

//***** Post Request for Login *****//
app.post('/', auth, (req, res)=> {
    const { error } = validateUserData(req.body);
    if(error) {
        var errors = {
            success:false,
            msg:error.name,
            data:error.details[0].message
        };
        res.send(errors);
        return;
    }
    
    unsubscribeStore(req.body).then((response)=> {
        if(response == 500) {
            var errors = {
                success:false,
                msg:'No User Found', 
                data:''
            };
            res.send(errors);
        }
        else {
            var success = {
                success:true,
                msg:'User Found', 
                data:_.pick(response, ['_id', 'userName', 'mobile', 'liked_stores', 'fullName', 'email', 'createdDate'])
            };
            res.send(success);
        }
    });
});
//***** ///// *****//

//***** User login data validation function *****//
function validateUserData(userData) {
    const schema = Joi.object().keys({
        userId: Joi.string(),
        storeId:Joi.string()
    });
    return Joi.validate(userData, schema);
}
//***** ///// *****//

//***** Find User and return function *****//
async function unsubscribeStore(body) {
    const user = await UserData.findById(body.userId);
    const partner = await PartnerData.findById(body.storeId);
    var tmp = null;
    for(var i=0; i < user.liked_stores.length; i++){
        if(user.liked_stores[i] == body.storeId) {
            user.liked_stores.splice(i, 1);
            tmp = await user.save();
            delete tmp.password;
            break;
        }
    }
    for(var j=0; j < partner.liked_by.length; j++){
        if(partner.liked_by[j] == body.userId) {
            partner.liked_by.splice(j, 1);
            temp = await partner.save();
            break;
        }
    }
    return(tmp);
    // const update = await UserData.updateOne(
    //     { _id: body.userId },
    //     { $pull: { liked_stores: body.storeId }
    //  },
    //  { safe: true, multi:true },
    //  function(err, obj){
    //      console.log("~~~err~~~");
    //      console.log(err);
    //      console.log('~~~OBJ~~~');
    //      console.log(obj);
    //  }
    // );
}
//***** ///// *****//

module.exports = app;