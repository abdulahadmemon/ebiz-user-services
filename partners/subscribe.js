//***** Modules goes here *****//
const express = require('express');
const Joi = require('joi');
const PartnerData = require('./schema');
const {UserData} = require('../userModel/schema');
const auth = require('../middleware/auth');
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express();
//***** ///// *****//

//***** Post Request for Login *****//

app.post('/', auth, (req, res)=> {
    const { error } = validatePartnerData(req.body);
    if(error) {
        var errors = {
            success:false,
            msg:error.name, 
            data:error.details[0].message
        };
        res.send(errors);
        return;
    }
    
    checkStore(req.body).then((response)=> {
        if(response == 3) {
            var errors = {
                success:false,
                msg:'Sorry, we cant find the store.',
                data:{}
            };
            res.send(errors);
        }
        else if(response == 2) {
            var errors = {
                success:false,
                msg:'You had already subscribed this store.',
                data:{}
            };
            res.send(errors);
        }
        else {
            var success = {
                success:true,
                msg:'Store added successfully.', 
                data:{}
            };
            res.send(success);
        }
    });
});
//***** ///// *****//

//***** User login data validation function *****//
function validatePartnerData(data) {
    const schema = Joi.object().keys({
        qrCode: Joi.string().required(),
        userId: Joi.string().required()
    });
    return Joi.validate(data, schema);
}
//***** ///// *****//

async function checkStore(body) {
    const store = await PartnerData
    .find({qrcodePath:new RegExp('.*'+ body.qrCode+'.png')});
    var updateCredits = store[0].credits + 1;

    const user = await UserData.update(
        { _id: body.userId }, 
        { $addToSet: { liked_stores: store[0]._id } }
    );
    
    if(user.ok == 1 && user.nModified == 1) {
        const tmp = await PartnerData.findByIdAndUpdate( store[0]._id,
            { $addToSet: { liked_by: body.userId } }    
        );
        const update = await PartnerData.findByIdAndUpdate( store[0]._id,
            { $set: { credits: updateCredits } }    
        );
        return 1; // 1 = store added
    }
    else if(user.ok == 1 && user.nModified == 0)
        return 2; // 2 = store already subscibed
    else
        return 3;
}

module.exports = app;