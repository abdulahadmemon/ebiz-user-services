const mongoose = require('mongoose');

//***** User Document Schema *****//
const partnerSchema = new mongoose.Schema({
    fullName: {
        type: String,
        min: 4,
        max: 30,
        required: true
    },
    userName: {
        type: String,
        min: 4,
        max: 30,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    mobile: {
        type: Number,
        required: true
    },
    password: {
        type: String,
        required: true,
        select:false
    },
    credits: {
        type: Number,
        required: true
    },
    gcm_id: String,
    platform: String,
    companyName:String,
    createdDate:{ type:Date, default:Date.now },
    liked_by:Array
});

const PartnerData = mongoose.model('shopkeepers', partnerSchema);
//***** ///// *****//

module.exports = PartnerData;