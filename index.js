//***** Modules goes here *****//
const config = require('config');
const debug_N = require('debug')('app:startup');
const debug_DB = require('debug')('app:db');
const express = require('express');
//***** ///// *****//

//***** Initialize express *****//
const app = express();
//***** ///// *****//

//***** Route all requests to route.js *****//
const routesModule = require('./routes');
app.use('/api', routesModule);
//***** ///// *****//

// const app = express();
// app.set('view engine', 'pug');
// app.set('views', './views'); //default


//***** Assigning Port *****//
//process.env.PORT || 3000
const port = parseInt(process.env.PORT);
app.listen(4201,()=>console.log(`Listening to port ${process.env.PORT}... :)`));
// app.listen(process.env.PORT, ()=> console.log(`Listening to port ${process.env.PORT}... :)`));
//***** ///// *****//