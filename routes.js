//***** Modules goes here *****//
const express = require('express');
const helmet = require('helmet');
const mongoose = require('mongoose');
const cors = require('cors');
//***** ///// *****//

//***** Connection to mongoDB *****//
// mongoose.connect('mongodb+srv://mudassir:HP6910pc@smartlybiz-v9pjm.mongodb.net?authSource=admin')
mongoose.connect('mongodb://localhost/smartlyBizDB')
.then(()=> console.log('connected to smartlyBizDB...'))
.catch((err)=> console.error('Could not connect to database...', err));
//***** ///// *****//

//***** Express Router to export in module *****//
const app = express.Router();
//***** ///// *****//

//***** Middlewares *****//
app.use(express.json());
// app.use(express.urlencoded({ extended:true }));
app.use(express.urlencoded({limit: '10mb',extended:true }));
// app.use(express.static('public_files'));
app.use(helmet());
app.use(cors());
//***** ///// *****//


//***** Distributing requests *****//
//~~ getUser, Signin, Signup ~~//
const RoutesModule = require('./userModel/userRoutes');
app.use('/user', RoutesModule);

//~~ getAllPartnersList ~~//
const PartnerModule = require('./partners/route');
app.use('/partner', PartnerModule);
//***** ///// *****//

//~~ Deals Module ~~//
const DealsModule = require('./dealsModel/dealRoutes');
app.use('/deals', DealsModule);
//***** ///// *****//

//~~ Message Module ~~//
const MessageModule = require('./messageModel/routes');
app.use('/message', MessageModule);
//***** ///// *****//

module.exports = app;